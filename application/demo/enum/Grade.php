<?php

namespace app\demo\enum;

final class Grade extends Enum {

    const ONE = 1;

    const TWO = 2;

    const THREE = 3;

    const FOUR = 4;

    public static function getTitleMap(): array {
        return [
            self::ONE => "一年级",
            self::TWO => "二年级",
            self::THREE => "三年级",
            self::FOUR => "四年级",
        ];
    }

}