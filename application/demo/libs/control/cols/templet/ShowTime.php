<?php

namespace app\demo\libs\control\cols\templet;

use yunj\control\cols\YunjCols;

class ShowTime extends YunjCols {

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // 定义额外配置项（无额外配置项可不写）
    protected function defineExtraArgs(): array {
        return [
            'format' => 'Y-m-d H:i:s',  // 时间格式
        ];
    }

    // 处理配置项（不需要处理可不写）
    protected function handleArgs(array $args): array {
        return $args;
    }

}