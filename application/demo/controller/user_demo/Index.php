<?php

namespace app\demo\controller\user_demo;

use app\demo\controller\Controller;
use app\demo\service\user_demo\Index as Service;

class Index extends Controller {

    public function lists() {
        $builder = YT('demo', [
            "state" => Service::getInstance()->state(),
            "filter" => function ($state) {
                return Service::getInstance()->filter($state);
            },
            "toolbar" => function ($state) {
                return Service::getInstance()->toolbar($state);
            },
            "defaultToolbar" => function ($state) {
                return Service::getInstance()->defaultToolbar($state);
            },
            "import" => url("import"),
            "cols" => function ($state) {
                return Service::getInstance()->cols($state);
            },
            "count" => function ($filter) {
                return Service::getInstance()->count($filter);
            },
            "items" => function ($limitStart, $limitLength, $filter, $sort) {
                return Service::getInstance()->items($limitStart, $limitLength, $filter, $sort);
            },
            "event" => function ($event, $ids) {
                $res = Service::getInstance()->event($event, $ids);
                return $res === true ? success_json() : error_json($res);
            }
        ]);
        $builder->assign($this);
        return $this->fetch();
    }

    public function add() {
        $builder = YF('demo', [
            "tab" => Service::getInstance()->tab(),
            "field" => function ($tab) {
                return Service::getInstance()->field($tab);
            },
            "button" => ['reload','reset', 'submit'],
            "submit" => function ($data) {
                $res = Service::getInstance()->submit($data);
                return $res ? success_json(['reload' => true]) : error_json();
            },
        ]);
        $builder->assign($this);
        return $this->fetch();
    }

    public function edit() {
        $builder = YF('demo', [
            "tab" => Service::getInstance()->tab(),
            "field" => function ($tab) {
                return Service::getInstance()->field($tab, true);
            },
            "button" => ['reload','reset', 'submit'],
            "load" => function () {
                return Service::getInstance()->load();
            },
            "submit" => function ($data) {
                $res = Service::getInstance()->submit($data, true);
                return $res ? success_json(['reload' => true]) : error_json("修改失败");
            },
        ]);
        $builder->assign($this);
        return $this->fetch();
    }

    public function userDropdownSearchOptions() {
        $items = Service::getInstance()->userDropdownSearchOptions();
        return success_json($items);
    }

    public function import() {
        $builder = YI('demo', [
            "sheet" => Service::getInstance()->sheet(),
            "cols" => function ($sheet) {
                return Service::getInstance()->importCols($sheet);
            },
            "rows" => function ($rowsData) {
                return Service::getInstance()->rows($rowsData);
            }
        ]);
        $builder->assign($this);
        return $this->fetch();
    }

}