<?php

namespace app\demo\controller\table_demo;

use app\demo\controller\Controller;
use app\demo\service\table_demo\Index as Service;

class Index extends Controller {

    public function chain() {
        $builder = YT('demo')
            ->state(Service::getInstance()->state())
            ->page(false)
            ->cols(function ($state) {
                return Service::getInstance()->cols($state);
            })
            ->count(function ($filter) {
                return Service::getInstance()->count($filter);
            })
            ->items(function ($limitStart, $limitLength, $filter, $sort) {
                return Service::getInstance()->items($limitStart, $limitLength, $filter, $sort);
            })
            ->event(function ($event, $ids) {
                return error_json("演示数据不能进行操作");
            });
        $builder->assign($this);
        return $this->fetch();
    }

    public function arrayConfig() {
        $builder = YT('demo', [
            "state" => Service::getInstance()->state(),
            "page" => false,
            "cols" => function ($state) {
                return Service::getInstance()->cols($state);
            },
            "count" => function ($filter) {
                return Service::getInstance()->count($filter);
            },
            "items" => function ($limitStart, $limitLength, $filter, $sort) {
                return Service::getInstance()->items($limitStart, $limitLength, $filter, $sort);
            },
            "event" => function ($event, $ids) {
                return error_json("演示数据不能进行操作");
            }
        ]);
        $builder->assign($this);
        return $this->fetch();
    }

    public function dragSort() {
        $builder = YT('demo', [
            "state" => Service::getInstance()->state(),
            "page" => false,
            "cols" => function ($state) {
                return Service::getInstance()->cols($state, true);
            },
            "count" => function ($filter) {
                return Service::getInstance()->count($filter);
            },
            "items" => function ($limitStart, $limitLength, $filter, $sort) {
                return Service::getInstance()->items($limitStart, $limitLength, $filter, $sort);
            },
            "event" => function ($event, $ids) {
                return error_json("演示数据不能进行操作");
            }
        ]);
        $builder->assign($this);
        return $this->fetch();
    }

}