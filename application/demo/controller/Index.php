<?php

namespace app\demo\controller;

class Index extends Controller {

    public function welcome() {
        return $this->fetch("../application/demo/view/index/welcome.html");
    }

}