<?php

namespace app\demo\controller\form_demo;

use app\demo\controller\Controller;
use app\demo\service\form_demo\Index as Service;

class Index extends Controller {

    public function chain() {
        $builder = YF('demo')
            ->tab(Service::getInstance()->tab())
            ->field(function ($tab) {
                return Service::getInstance()->field($tab);
            })
            ->button(['reload', 'reset', 'submit'])
            ->submit(function ($data) {
                // $data 表单数据

                // ...业务代码

                // 返回相应结果
                return success_json();
            });
        $builder->assign($this);
        return $this->fetch();
    }

    public function arrayConfig() {
        $builder = YF('demo', [
            "tab" => Service::getInstance()->tab(),
            "field" => function ($tab) {
                return Service::getInstance()->field($tab);
            },
            "button" => ['reload', 'reset', 'submit'],
            "submit" => function ($data) {
                // $data 表单数据

                // ...业务代码

                // 返回相应结果
                return success_json();
            },
        ]);
        $builder->assign($this);
        return $this->fetch();
    }

    public function dropdownSearchOptions() {
        $items = Service::getInstance()->dropdownSearchOptions();
        return success_json($items);
    }

    public function fieldValidate(){
        $builder = YF('demo', [
            "tab" => Service::getInstance()->tab(),
            "field" => function ($tab) {
                return Service::getInstance()->field($tab);
            },
            "button" => ['reload', 'reset', 'submit'],
            "submit" => function ($data) {
                // $data 表单数据

                // ...业务代码

                // 返回相应结果
                return success_json();
            },
        ]);
        $builder->assign($this);
        return $this->fetch();
    }

}