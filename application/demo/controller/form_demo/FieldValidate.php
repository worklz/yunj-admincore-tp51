<?php

namespace app\demo\controller\form_demo;

use app\demo\controller\Controller;
use app\demo\validate\form_demo\FieldValidate as Validate;

class FieldValidate extends Controller {

    public function index() {
        $builder = YF('demo', [
            "tab" => ["base" => "基础"],
            "field" => function ($tab) {
                $field = [
                    "name" => [
                        "title" => "姓名",
                        "type" => "text",
                        "verify" => "require",
                        "desc" => "必填",
                    ],
                    "desc" => [
                        "title" => "介绍",
                        "type" => "textarea",
                        "verify" => "max:200",
                        "desc" => "限定200字符",
                    ],
                    "username" => [
                        "title" => "账户",
                        "type" => "text",
                        "verify" => "require|alphaDash",
                        "desc" => "必填，限定字母/数字/下划线_及短横线-组合",
                    ],
                    "custom" => [
                        "title" => "自定义字段",
                        "type" => "text",
                        "verify" => "checkCustom",
                        "desc" => "自定义方法checkCustom，校验在输入内容时只能是：123456",
                    ]
                ];
                return $field;
            },
            "fieldValidate" => Validate::class,
            "button" => ['reload', 'reset', 'submit'],
            "submit" => function ($data) {
                return success_json();
            },
        ]);
        $builder->assign($this);
        return $this->fetch();
    }

    public function codePreview(){
        return $this->fetch();
    }

}