<?php

namespace app\demo\controller\view_demo;

use app\demo\controller\Controller;

class Index extends Controller {

    public function index() {
        return $this->fetch();
    }

    public function icon() {
        $icon = [
            [
                "title" => "订阅",
                "class" => "yunj-icon-sub"
            ],
            [
                "title" => "时间",
                "class" => "yunj-icon-time"
            ],
            [
                "title" => "分类",
                "class" => "yunj-icon-category"
            ],
            [
                "title" => "眼睛",
                "class" => "yunj-icon-eye"
            ],
            [
                "title" => "笔",
                "class" => "yunj-icon-pen"
            ],
            [
                "title" => "列表",
                "class" => "yunj-icon-list"
            ],
            [
                "title" => "清扫/清空",
                "class" => "yunj-icon-clear"
            ],
            [
                "title" => "提交",
                "class" => "yunj-icon-submit"
            ],
            [
                "title" => "返回",
                "class" => "yunj-icon-back"
            ],
            [
                "title" => "重置",
                "class" => "yunj-icon-refresh"
            ],
            [
                "title" => "排序",
                "class" => "yunj-icon-sort"
            ],
            [
                "title" => "排序(圈)",
                "class" => "yunj-icon-sort-circle"
            ],
        ];
        $this->assign("icon", $icon);
        return $this->fetch();
    }

}