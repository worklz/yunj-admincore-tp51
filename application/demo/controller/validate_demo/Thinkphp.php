<?php

namespace app\demo\controller\validate_demo;

use app\demo\controller\Controller;
use app\demo\service\validate_demo\Thinkphp as Service;

class Thinkphp extends Controller {

    public function index() {
        if (request()->isAjax()) {
            $res = Service::getInstance()->handleData();
            return $res === true ? success_json() : error_json($res);
        }
        return $this->fetch();
    }

}