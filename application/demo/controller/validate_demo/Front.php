<?php

namespace app\demo\controller\validate_demo;

use app\demo\controller\Controller;

class Front extends Controller {

    public function index() {
        return $this->fetch();
    }

}