<?php

namespace yunj\controller;

use yunj\Controller;

final class Index extends Controller {

    public function index() {
        $template = view_template("admin/index");
        return view($template);
    }

}