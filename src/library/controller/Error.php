<?php

namespace yunj\controller;

use yunj\Controller;
use yunj\enum\TipsTemplet;

final class Error extends Controller {

    public function tips() {
        $param = input('get.') ?: [];
        $templet = isset($param['templet'])?(int)$param['templet']:null;
        /** @var TipsTemplet $templet */
        $templet = $templet && TipsTemplet::isValue($templet) ? TipsTemplet::get($templet) : TipsTemplet::ERROR();
        $data = [
            "code" => $templet->getValue(),
            "msg" => $param['msg'] ?? $templet->getMsg(),
            "action" => $templet->getAction()
        ];
        if ($data['code'] == 200) $data['msg'] .= '当前时间：' . date('Y-m-d H:i:s');
        return view(view_template("admin/error/tips"), $data);
    }

    public function _empty() {
        if (request()->isAjax()) {
            return success_json();
        }
        die('empty');
    }

}