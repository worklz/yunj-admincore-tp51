<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2020 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1732983738@qq.com>
// +----------------------------------------------------------------------
// | 文件相关配置，默认文件内容
// +----------------------------------------------------------------------

return [
    "upload_img_ext" => <<<INFO
    // 上传图片后缀
    'upload_img_ext'=>'jpg,png,gif,jpeg',
INFO
    ,
    "upload_img_size" => <<<INFO
    // 上传图片大小（单位MB）
    'upload_img_size'=>1,
INFO
    ,
    "upload_file_ext" => <<<INFO
    // 上传文件后缀
    'upload_file_ext'=>'txt,pdf,xlsx,xls,csv',
INFO
    ,
    "upload_file_size" => <<<INFO
    // 上传文件大小（单位MB）
    'upload_file_size'=>5,
INFO
    ,
    "upload_media_ext" => <<<INFO
    // 上传媒体后缀
    'upload_media_ext'=>'avi,mkv,mp3,mp4',
INFO
    ,
    "upload_media_size" => <<<INFO
    // 上传媒体大小（单位MB）
    'upload_media_size'=>20,
INFO
    ,
];