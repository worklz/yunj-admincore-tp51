<?php

namespace yunj\enum;

final class TipsTemplet extends Enum {

    const NOT_FOUND = 404;

    const LOGOUT = 200;

    const OVERDUE = 401;

    const NO_AUTH = 403;

    const TIMEOUT = 408;

    const ERROR = 500;

    public static function getMsgMap(): array {
        return [
            self::NOT_FOUND => "不好意思，您访问的页面不存在~",
            self::LOGOUT => "您已成功退出平台！",
            self::OVERDUE => "不好意思，您的登录已过期~",
            self::NO_AUTH => "不好意思，您无权限访问...",
            self::TIMEOUT => "不好意思，您的操作已超时，请重新登录~",
            self::ERROR => "系统内部异常，稍后重试~",
        ];
    }

    public function getMsg(){
        return $this->match(static::getMsgMap());
    }

    public static function getActionMap(): array {
        return [
            self::NOT_FOUND => "__BACK__",
            self::LOGOUT => "__LOGIN__",
            self::OVERDUE => "__LOGIN__",
            self::NO_AUTH => "__BACK__",
            self::TIMEOUT => "__LOGIN__",
            self::ERROR => "__BACK__",
        ];
    }

    public function getAction(){
        return $this->match(static::getActionMap());
    }

    /**
     * @return array
     * @deprecated 弃用
     */
//    public static function options(): array {
//        $options = [
//            self::NOT_FOUND => [
//                'msg' => '不好意思，您访问的页面不存在~',
//                'action' => '__BACK__',
//            ],
//            self::LOGOUT => [
//                'msg' => '您已成功退出平台！',
//                'action' => '__LOGIN__',
//            ],
//            self::OVERDUE => [
//                'msg' => '不好意思，您的登录已过期~',
//                'action' => '__LOGIN__',
//            ],
//            self::NO_AUTH => [
//                'msg' => '不好意思，您无权限访问...',
//                'action' => '__BACK__',
//            ],
//            self::TIMEOUT => [
//                'msg' => '不好意思，您的操作已超时，请重新登录~',
//                'action' => '__LOGIN__',
//            ],
//            self::ERROR => [
//                'msg' => '系统内部异常，稍后重试~',
//                'action' => '__BACK__',
//            ],
//        ];
//        return $options;
//    }
//
//    /**
//     * @deprecated 弃用
//     */
//    public static function assignData() {
//        $param = input('get.') ?: [];
//        $templet = isset($param['templet']) && self::isExist($param['templet']) ? $param['templet'] : self::ERROR;
//        $templetAttr = self::attr($templet);
//        $assignData = ["code" => $templet] + $templetAttr;
//        if (isset($param['msg'])) $assignData['msg'] = $param['msg'];
//        if ($assignData['code'] == 200) $assignData['msg'] .= '当前时间：' . date('Y-m-d H:i:s');
//        return $assignData;
//    }

}