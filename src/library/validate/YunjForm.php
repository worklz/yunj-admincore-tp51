<?php

namespace yunj\validate;

use yunj\Validate;

class YunjForm extends Validate {

    protected $rule = [
        'builderId' => 'require',
        'builderAsyncType' => 'require|in:load,submit',
    ];

    protected $message = [
        'builderId.require' => '访问错误',
        'builderAsyncType.require' => '[builderAsyncType]参数缺失',
        'builderAsyncType.in' => '[builderAsyncType]参数错误',
    ];

    protected $scene = [
        'AsyncRequest' => ['builderId', 'builderAsyncType'],
    ];

    protected function handleData(array $rawData, $scene): array {
        $data = $rawData;
        if ($scene == 'AsyncRequest') {
            switch ($rawData['builderAsyncType']) {
                case 'load':
                    $data = $this->handleDataByLoad($rawData);
                    break;
                case 'submit':
                    $data = $this->handleDataBySubmit($rawData);
                    break;
            }
        }
        return $data;
    }

    private function handleDataByLoad($rawData) {
        $data = $rawData;
        return $data;
    }

    private function handleDataBySubmit($rawData) {
        $data = $rawData;
        if (!isset($rawData['data'])) throw_error_json('[data]参数缺失');
        $data['data'] = json_decode($rawData['data'], true);
        return $data;
    }
}