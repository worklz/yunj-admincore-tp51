<?php
namespace yunj\validate;

use yunj\Validate;

class Qiniu extends Validate {

    protected $rule = [
        'is_enable' => 'require|bool',
        'access_key' => 'require',
        'secret_key' => 'require',
        'bucket' => 'require',
        'domain' => 'require|formatDomain',
    ];

    protected $message = [
        'is_enable.require' => '七牛云[is_enable]配置缺失',
        'is_enable.bool' => '七牛云[is_enable]配置数据类型为布尔类型',
        'access_key.require' => '七牛云[access_key]配置缺失',
        'secret_key.require' => '七牛云[secret_key]配置缺失',
        'bucket.require' => '七牛云[bucket]配置缺失',
        'domain.require' => '七牛云[domain]配置缺失',
    ];

    // 格式化域名
    protected function formatDomain($value,$rule='',$data){
        $this->data['domain']=substr($value, -1)=='/'?substr($value, 0,-1):$value;
        return true;
    }

    protected $scene = [
        'config'=>['is_enable','access_key','secret_key','bucket','domain'],
    ];

}