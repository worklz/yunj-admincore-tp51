<?php
namespace yunj\validate;

use yunj\Validate;

class YunjImport extends Validate {

    protected $rule = [
        'builderId' => 'require',
        'builderAsyncType' => 'require|in:import',
        'items' => 'require|array',
    ];

    protected $message = [
        'builderId.require' => '访问错误',
        'builderAsyncType.require' => '[builderAsyncType]参数缺失',
        'builderAsyncType.in' => '[builderAsyncType]参数错误',
        'items.require' => '[builderAsyncType]参数缺失',
        'items.array' => '[items]参数错误',
    ];

    protected $scene = [
        'AsyncRequest'=>['builderId',"builderAsyncType",'items'],
    ];

    protected function handleData(array $rawData,$scene): array {
        $data=$rawData;
        if($scene=='AsyncRequest'){
            switch ($data['builderAsyncType']){
                case "import":
                    $this->handleDataByImport($data);
                    break;
            }
        }
        return $data;
    }

    private function handleDataByImport(&$data){
        $data["count"] = count($data['items']);
        return $data;
    }

}