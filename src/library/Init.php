<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2020 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1732983738@qq.com>
// +----------------------------------------------------------------------
// | 注意：
// | 1、配置 'url_common_param'=>true
// +----------------------------------------------------------------------
// | 初始化（禁止更改代码顺序）
// +----------------------------------------------------------------------

namespace yunj;

use yunj\upgrade\Upgrade;

class Init {

    public function appInit($params) {
        $this->init();
        $this->check();
        $this->cors();
    }

    // 初始化
    private function init() {
        // 检测PHP环境
        if (version_compare(PHP_VERSION, '7.1', '<')) die('PHP版本过低，最少需要PHP7.1，请升级PHP版本！');

        // 短路径
        if (!defined('YUNJ_VENDOR_SHORT_PATH')) define('YUNJ_VENDOR_SHORT_PATH', "vendor/yunj/admincore-tp51/");
        // 根目录路径
        if (!defined('YUNJ_VENDOR_PATH')) define('YUNJ_VENDOR_PATH', env("root_path") . YUNJ_VENDOR_SHORT_PATH);
        // src路径
        if (!defined('YUNJ_VENDOR_SRC_PATH')) define('YUNJ_VENDOR_SRC_PATH', YUNJ_VENDOR_PATH . "src/");
        // 版本号
        if (!defined('YUNJ_VERSION')) define('YUNJ_VERSION', include YUNJ_VENDOR_SRC_PATH . "version.php");
        // path
        if (!defined('YUNJ_PATH')) define('YUNJ_PATH', env("root_path") . "yunj/");

        // 引入路由文件
        include YUNJ_VENDOR_SRC_PATH . 'route.php';
    }

    // 校验
    private function check(): void {
        // 启用检测：是否支持多模块
        if (config("app.app_multi_module") !== true) die("是否支持多模块配置[app.app_multi_module]未启用");
        // 检查是否为开发环境，开发环境不进行升级
        $rootPath = env("root_path");
        $isDevPath1 = $rootPath . "version.php";
        $isDevPath2 = $rootPath . "uglifyjs.php";
        $isDevPath3 = $rootPath . "cmpcss.php";
        $isDevPath4 = $rootPath . "git.php";
        if (file_exists($isDevPath1) && file_exists($isDevPath2) && file_exists($isDevPath3) && file_exists($isDevPath4)) return;
        // 升级检查
        Upgrade::check();
    }

    // CORS跨域请求处理
    private function cors() {
        $origin = yunj_config('cors.access_control_allow_origin', '*');
        $origin = $origin ?: '*';
        $methods = yunj_config('cors.access_control_allow_methods', '');
        $methods = $methods && (substr($methods, 0, 1) !== ',') ? ',' . $methods : $methods;
        $headers = yunj_config('cors.access_control_allow_headers', '');
        $headers = $headers && (substr($headers, 0, 1) !== ',') ? ',' . $headers : $headers;
        header('Access-Control-Allow-Origin:' . $origin);
        header('Access-Control-Allow-Methods:POST,GET,PUT,DELETE,OPTIONS' . $methods);
        header("Access-Control-Allow-Headers:token,Origin,X-Requested-With,Content-Type,Accept" . $headers);
        if (request()->isOptions()) exit();
    }

}