<?php

namespace yunj\control\field;

class Markdown extends YunjField {

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs(): array {
        return [
            'mode' => 'editormd',           // 模式（可选值：editormd（默认））
            "modeConfig" => [               // 模式的配置
                "editormd" => [
                    "height" => 250,
                    "watch" => false,
                    "placeholder" => "此处开始编写...",
                    "imageFormats" => explode(",", yunj_config('file.upload_img_ext')),
                    // 全屏展开编辑有bug
                    "toolbar" => [
                        "undo", "redo", "|", "bold", "del", "italic", "quote", "|"
                        , "h1", "h2", "h3", "h4", "|", "list-ul", "list-ol", "hr", "|"
                        , "align-left", "align-center", "align-right", "align-justify", "|"
                        , "table", "datetime", "html-entities", "pagebreak", "code", "code-block", "|"
                        , "link", "reference-link", "image", "video", "|"
                        , "watch", "preview", /*"fullscreen",*/
                        "clear", "search", "|", "help"
                    ]
                ],
            ],
            'readonly' => false,
        ];
    }

    protected function handleArgs($args):array {
        $mode = $args["mode"];
        $modeConfig = $args["modeConfig"];
        $defaultModeConfig = $this->defineExtraArgs()["modeConfig"];
        // 没有设置配置
        if (!isset($modeConfig[$mode])) {
            $modeConfig[$mode] = $defaultModeConfig[$mode];
            $args["modeConfig"] = $modeConfig;
            return $args;
        }
        // 有配置
        $modeConfig[$mode] += $defaultModeConfig[$mode];
        if ($mode === "editormd") {
            // 若果是editormd，对toolbar取交集
            $modeConfig[$mode]["toolbar"] = array_intersect($modeConfig[$mode]["toolbar"], $defaultModeConfig[$mode]["toolbar"]);
        }
        $args["modeConfig"] = $modeConfig;
        return $args;
    }

}