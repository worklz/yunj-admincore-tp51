<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2020 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1732983738@qq.com>
// +----------------------------------------------------------------------
// | 路由文件
// +----------------------------------------------------------------------

use think\facade\Route;

// 必要中间件过滤
Route::group('admin/', function () {
    Route::get('', "\\yunj\\controller\\Index@index");
    Route::get('welcome', yunj_config("admin.welcome_route"));
    Route::rule('file/upload', "\\yunj\\controller\\File@upload");
    Route::rule('empty', "\\yunj\\controller\\Error@_empty");
})->middleware(yunj_config("admin.middleware", []));

// 不用中间件过滤
Route::group('admin/', function () {
    Route::get('tips', "\\yunj\\controller\\Error@tips");
});
