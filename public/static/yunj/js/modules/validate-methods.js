/**
 * validateMethods（验证器方法）
 */
layui.define(['yunj'], function (exports) {

    let validateMethods = {

        // 必填
        method_require: (value, rule = '', data = {}) => {
            if ((!value && value !== 0) || value === null || yunj.isUndefined(value) || yunj.isEmptyString(value) || yunj.isEmptyArray(value) || yunj.isEmptyObj(value)) return '不能为空';
            return true;
        },

        // 字符串中的字符是否都是数字
        method_number: (value, rule = '', data = {}) => {
            let reg = /^[0-9]*$/;
            return reg.test(value) ? true : '需由数字组成';
        },

        // 整数
        method_integer: (value, rule = '', data = {}) => {
            let reg = /^-?[1-9]\d*$/;
            return reg.test(value) ? true : '需为整数';
        },

        // 正整数
        method_positiveInteger: (value, rule = '', data = {}) => {
            if (yunj.isPositiveInteger(value)) return true;
            return '需为正整数';
        },

        // 正整数
        method_positiveInt: (value, rule = '', data = {}) => {
            if (yunj.isPositiveInt(value)) return true;
            return '需为正整数';
        },

        // 非负整数
        method_nonnegativeInteger: (value, rule = '', data = {}) => {
            let reg = /^[1-9]\d*|0$/;
            return reg.test(value) ? true : '需为非负整数';
        },

        // 浮点数
        method_float: (value, rule = '', data = {}) => {
            return yunj.isFloat(value) ? true : '需为浮点数';
        },

        // 布尔值
        method_boolean: (value, rule = '', data = {}) => {
            return yunj.isBool(value) ? true : '需为布尔值';
        },

        // 布尔值
        method_bool: (value, rule = '', data = {}) => {
            return yunj.isBool(value) ? true : '需为布尔值';
        },

        // 长度是否在某个范围
        method_length: (value, rule = '', data = {}) => {
            if (!rule || rule.length <= 0) return `验证规则[length]错误`;
            let valLen = value.toString().length;
            if (rule.indexOf(',') === -1) {
                let limitLen = rule | 0;
                if (validateMethods.method_positiveInteger(limitLen) !== true) return `验证规则[length]参数需为正整数错误`;
                return valLen === limitLen ? true : `限制长度 ${limitLen}`;
            }
            let limitLenArr = rule.split(',');
            if (limitLenArr.length !== 2) return '验证规则[length]参数需为“,”间隔的两个正整数';
            let [limitMinLen, limitMaxLen] = limitLenArr;
            if (validateMethods.method_positiveInteger(limitMinLen) !== true || validateMethods.method_positiveInteger(limitMaxLen) !== true) return '验证规则[length]参数需为“,”间隔的两个正整数';
            return valLen >= limitMinLen && valLen <= limitMaxLen ? true : `限制长度区间 ${limitMinLen}-${limitMaxLen}`;
        },

        // 最小长度
        method_min: (value, rule = '', data = {}) => {
            let minLen = rule | 0;
            let valLen = value.toString().length;
            return valLen >= minLen ? true : `限制最小长度 ${minLen}`;
        },

        // 最大长度
        method_max: (value, rule = '', data = {}) => {
            let maxLen = rule | 0;
            let valLen = value.toString().length;
            return valLen <= maxLen ? true : `限制最大长度 ${maxLen}`;
        },

        // 是否在某个范围
        method_in: (value, rule = '', data = {}) => {
            if (!rule) return '验证规则[in]错误';
            rule = rule.split(',');
            if (rule.length <= 0) return '验证规则[in]错误';
            return rule.indexOf(value) !== -1 ? true : '错误';
        },

        // 是否不在某个范围
        method_notIn: (value, rule = '', data = {}) => {
            if (!rule) return '验证规则[notIn]错误';
            rule = rule.split(',');
            if (rule.length <= 0) return '验证规则[notIn]错误';
            return rule.indexOf(value) === -1 ? true : "错误";
        },

        // 是否在某个区间
        method_between: (value, rule = '', data = {}) => {
            if (!rule) return '验证规则[between]错误';
            rule = rule.split(',');
            if (rule.length !== 2) return '验证规则[between]错误';
            let [min, max] = rule;
            return value >= min && value <= max ? true : '错误';
        },

        // 是否不在某个区间
        method_notBetween: (value, rule = '', data = {}) => {
            if (!rule) return '验证规则[notBetween]错误';
            rule = rule.split(',');
            if (rule.length !== 2) return '验证规则[notBetween]错误';
            let [min, max] = rule;
            return value < min || value > max ? true : "错误";
        },

        // 是否等于某个值
        method_eq: (value, rule = '', data = {}) => {
            if (!rule || rule.length <= 0) return '验证规则[eq]错误';
            return (value | 0) === (rule | 0) ? true : "错误";
        },

        // 是否大等于某个值
        method_egt: (value, rule = '', data = {}) => {
            if (!rule || rule.length <= 0) return '验证规则[egt]错误';
            return (value | 0) >= (rule | 0) ? true : "错误";
        },

        // 是否大于某个值
        method_gt: (value, rule = '', data = {}) => {
            if (!rule || rule.length <= 0) return '验证规则[gt]错误';
            return (value | 0) > (rule | 0) ? true : "错误";
        },

        // 是否小等于某个值
        method_elt: (value, rule = '', data = {}) => {
            if (!rule || rule.length <= 0) return '验证规则[gt]错误';
            return (value | 0) <= (rule | 0) ? true : "错误";
        },

        // 是否小于某个值
        method_lt: (value, rule = '', data = {}) => {
            if (!rule || rule.length <= 0) return '验证规则[gt]错误';
            return (value | 0) < (rule | 0) ? true : "错误";
        },

        // 数组
        method_array: (value, rule = '', data = {}) => {
            return yunj.isArray(value) ? true : '格式错误';
        },

        // 一维数组里面的值只能为rule的值
        method_arrayIn: (value, rule = '', data = {}) => {
            if (!yunj.isArray(value)) return "格式错误";
            if (yunj.isEmptyArray(value)) return "不能为空";
            if (!rule || rule.indexOf(',') === -1) return '验证规则[arrayIn]错误';
            rule = rule.split(',');
            return yunj.array_in(value, rule) ? true : '错误';
        },

        // 一维数组为空或者里面的值只能为rule的值
        method_arrayEmptyOrIn: (value, rule = '', data = {}) => {
            if (!yunj.isArray(value)) return "格式错误";
            if (yunj.isEmptyArray(value)) return true;
            if (!rule || rule.indexOf(',') === -1) return '验证规则[arrayEmptyOrIn]错误';
            rule = rule.split(',');
            return yunj.array_in(value, rule) ? true : '错误';
        },

        // 数组元素为正整数
        method_arrayPositiveInt: (value, rule = '', data = {}) => {
            if (!yunj.isArray(value)) return "格式错误";
            if (value.length <= 0) return "不能为空";
            return yunj.isPositiveIntArray(value) ? true : "需为正整数数组";
        },

        // 数组为空或者元素为正整数
        method_arrayEmptyOrPositiveInt: (value, rule = '', data = {}) => {
            if (!yunj.isArray(value)) return "格式错误";
            if (yunj.isEmptyArray(value)) return true;
            return yunj.isPositiveIntArray(value) ? true : "需为正整数数组";
        },

        // 一维数组里面的值只能为rule的值
        method_mapHas: (value, rule = '', data = {}) => {
            if (!yunj.isObj(value)) return "格式错误";
            if (yunj.isEmptyObj(value)) return "不能为空";
            if (!rule || rule.indexOf(',') === -1) return '验证规则[mapHas]错误';
            rule = rule.split(',');
            let valueKeys = Object.keys(value);
            return yunj.arrayDiff(rule, valueKeys).length <= 0 ? true : '错误';
        },

        // 一维数组为空或者里面的值只能为rule的值
        method_mapEmptyOrHas: (value, rule = '', data = {}) => {
            if (!yunj.isObj(value)) return "格式错误";
            if (yunj.isEmptyObj(value)) return true;
            if (!rule || rule.indexOf(',') === -1) return '验证规则[mapEmptyOrHas]错误';
            rule = rule.split(',');
            let valueKeys = Object.keys(value);
            return yunj.arrayDiff(rule, valueKeys).length <= 0 ? true : '错误';
        },

        // 地区有效性
        method_area: (value, rule = '', data = {}) => {
            let accArr = ['province', 'city', 'district'];
            if (rule.length <= 0) rule = "district";
            if (accArr.indexOf(rule) === -1) return '验证规则 area 允许参数[province|city|district]';
            if (!yunj.hasOwnProperty('attr_area_options')) return '地区组件未加载';
            let options = yunj.attr_area_options;
            let acc = rule;
            if (!yunj.isObj(value)) return '数据异常';

            if (!value.hasOwnProperty('province') || value.province.toString().length <= 0) return '请选择省份';
            let provinceOptions = options[0];
            if (!provinceOptions.hasOwnProperty(value.province)) return '省份数据异常';
            if (acc === 'province') return true;

            if (!value.hasOwnProperty('city') || value.city.toString().length <= 0) return '请选择城市';
            let cityOptions = options[`0,${value.province}`];
            if (!cityOptions.hasOwnProperty(value.city)) return '城市数据异常';
            if (acc === 'city') return true;

            if (!value.hasOwnProperty('district') || value.district.toString().length <= 0) return '请选择区/县';
            let districtOptions = options[`0,${value.province},${value.city}`];
            if (!districtOptions.hasOwnProperty(value.district)) return '区/县数据异常';

            return true;
        },

        // 手机
        method_mobile: (value, rule = '', data = {}) => {
            let reg = /^1[3-9]\d{9}$/;
            return reg.test(value) ? true : '需为11位有效手机格式';
        },

        // 邮箱
        method_email: (value, rule = '', data = {}) => {
            let reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
            return reg.test(value) ? true : '需为有效邮箱格式';
        },

        // 汉字
        method_chs: (value, rule = '', data = {}) => {
            let reg = /^[\u4e00-\u9fa5]+$/;
            return reg.test(value) ? true : '只能是汉字';
        },

        // 汉字/字母/数字
        method_chsAlphaNum: (value, rule = '', data = {}) => {
            let reg = /^[\u4e00-\u9fa5a-zA-Z0-9]+$/;
            return reg.test(value) ? true : '只能是汉字/字母/数字';
        },

        // 汉字/字母/数字/下划线_/破折号-
        method_chsDash: (value, rule = '', data = {}) => {
            let reg = /^[\u4e00-\u9fa5a-zA-Z0-9\_\-]+$/;
            return reg.test(value) ? true : '只能是汉字/字母/数字/下划线_/破折号-';
        },

        // 汉字/字母/数字/下划线_/短横线-/空格
        method_chsDashSpace(value, rule = '', data = {}) {
            let reg = /^[\u4e00-\u9fa5a-zA-Z0-9\_\-\s]+$/;
            return reg.test(value) ? true : '只能是汉字、字母、数字、下划线_、短横线-及空格组合';
        },

        // 字母/数字
        method_alphaNum(value, rule = '', data = {}) {
            let reg = /^[A-Za-z0-9]+$/;
            return reg.test(value) ? true : '字母/数字';
        },

        // 字母/数字/下划线_/短横线-
        method_alphaDash(value, rule = '', data = {}) {
            let reg = /^[A-Za-z0-9\-\_]+$/;
            return reg.test(value) ? true : '字母/数字/下划线_/短横线-';
        },

        // 字母/数字/下划线_/短横线-
        method_hexColor(value, rule = '', data = {}) {
            let reg = /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/;
            return reg.test(value) ? true : '错误';
        },

        // 日期 yyyy-MM-dd 格式
        method_date(value, rule = '', data = {}) {
            return yunj.isString(value) && (new Date(value).getDate() === parseInt(value.substr(-2))) ? true : "错误";
        },

        // 日期事件 yyyy-MM-dd HH:mm:ss 格式
        method_datetime(value, rule = '', data = {}) {
            return yunj.isString(value) && (new Date(value).getSeconds() === parseInt(value.substr(-2))) ? true : "错误";
        },

        // 日期事件 yyyy-MM 格式
        method_month(value, rule = '', data = {}) {
            return yunj.isString(value) && ((new Date(value).getMonth() + 1) === parseInt(value.substr(-2))) ? true : "错误";
        },

        // 日期事件 HH:mm:ss 格式
        method_time(value, rule = '', data = {}) {
            if (!yunj.isString(value)) return "错误";
            value = `2021-11-26 ${value}`;
            return (new Date(value).getSeconds() === parseInt(value.substr(-2))) ? true : "错误";
        },

        // 日期事件 HH:mm:ss 格式
        method_year(value, rule = '', data = {}) {
            if (!yunj.isString(value)) return "错误";
            value = `${value}-01-01 00:00:01`;
            return (new Date(value).getFullYear() === parseInt(value.substr(0, 4))) ? true : "错误";
        },

        // 逗号“,”间隔的汉字/字母/数字组合
        method_commaIntervalChsAlphaNum: (value, rule = '', data = {}) => {
            let reg = /^[\u4e00-\u9fa5a-zA-Z0-9]+(?:,[\u4e00-\u9fa5a-zA-Z0-9]+)*$/;
            return reg.test(value) ? true : '错误';
        },

        // 逗号“,”间隔的正整数组合
        method_commaIntervalPositiveInt: (value, rule = '', data = {}) => {
            let reg = /^[1-9]\d*(?:,[1-9]\d*)*$/;
            return reg.test(value) ? true : '错误';
        },

        // url地址
        method_url: (value, rule = '', data = {}) => {
            let reg = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\*\+,;=.]+$/;
            return reg.test(value) ? true : '错误';
        },

    };

    exports('validateMethods', validateMethods);
});