/**
 * tableToolbar
 */
layui.define(['TableTemplet', 'jquery', 'dropdown'], function (exports) {

    let doc = document;
    let $ = layui.jquery;
    let dropdown = layui.dropdown;
    let TableTemplet = layui.TableTemplet;

    class TableToolbar extends TableTemplet {

        constructor(options) {
            options = Object.assign({}, {
                tableId: "",
                state: "",
                key: "",
                options: []
            }, options);
            super(options);
            this.actionOptions = {};     // 操作项
        }

        async renderBefore() {
            let that = this;
            that.actionOptions = that.options.options;
            return 'done';
        }

        layout() {
            let that = this;
            if (that.actionOptions.length <= 0) return false;
            if ($(doc).find(`body #${that.id}`).length > 0) return that.id;

            // 生成结构
            let templet = '';
            let actionOptions = that.actionOptions;
            if (yunj.isMobile()) {
                for (let k in actionOptions) {
                    if (!actionOptions.hasOwnProperty(k)) continue;
                    let option = actionOptions[k];
                    let c = option.class ? option.class : '';
                    let a = JSON.stringify(option);
                    let e = !option.hasOwnProperty("type") ? "asyncEvent"
                        : (["openTab", "openPopup"].indexOf(option.type) === -1 ? "asyncEvent" : option.type);
                    templet += `<dd class="${c}" lay-event="${e}" data-args='${a}'>${option.title}</dd>`;
                }
                templet = dropdown.layout(templet);
            } else {
                let itemTemplet = '';
                let dropdownTemplet = '';
                for (let k in actionOptions) {
                    if (!actionOptions.hasOwnProperty(k)) continue;
                    let option = actionOptions[k];
                    let c = option.class ? option.class : '';
                    let a = JSON.stringify(option);
                    let e = !option.hasOwnProperty("type") ? "asyncEvent"
                        : (["openTab", "openPopup"].indexOf(option.type) === -1 ? "asyncEvent" : option.type);
                    option.dropdown ? dropdownTemplet += `<dd class="${c}" lay-event="${e}" data-args='${a}'>${option.title}</dd>` : itemTemplet += `<button type="button" class="layui-btn layui-btn-xs layui-btn-primary ${c}" lay-event="${option.type}" data-args='${a}'>${option.title}</button>`;
                }
                if (itemTemplet) templet += `<div class="layui-btn-group" style="margin-right: 10px">${itemTemplet}</div>`;
                if (dropdownTemplet) templet += dropdown.layout(dropdownTemplet);
            }

            return templet;
        }

    }

    let tableToolbar = (args) => {
        return new TableToolbar(args);
    };

    exports('tableToolbar', tableToolbar);
});