/**
 * TableColAction
 */
layui.define(['TableColTemplet', 'dropdown'], function (exports) {

    let TableColTemplet = layui.TableColTemplet;
    let dropdown = layui.dropdown;

    class TableColAction extends TableColTemplet {

        constructor(options) {
            super(options);
        }

        layout() {
            let that = this;
            let options = that.args.options;
            if (options.length <= 0) return '';

            let templet = '';
            if (yunj.isMobile()) {
                for (let i = 0, l = options.length; i < l; i++) {
                    let option = options[i];
                    let c = option.class ? option.class : '';
                    let a = JSON.stringify(option);
                    let e = !option.hasOwnProperty("type") ? "asyncEvent"
                        : (["openTab", "openPopup"].indexOf(option.type) === -1 ? "asyncEvent" : option.type);
                    templet += `<dd class="${c}" lay-event="${e}" data-args='${a}'>${option.title}</dd>`;
                }
                templet = dropdown.layout(templet);
            } else {
                let itemTemplet = '';
                let dropdownTemplet = '';
                for (let i = 0, l = options.length; i < l; i++) {
                    let option = options[i];
                    let c = option.class ? option.class : '';
                    let a = JSON.stringify(option);
                    let e = !option.hasOwnProperty("type") ? "asyncEvent"
                        : (["openTab", "openPopup"].indexOf(option.type) === -1 ? "asyncEvent" : option.type);
                    option.dropdown ? dropdownTemplet += `<dd class="${c}" lay-event="${e}" data-args='${a}'>${option.title}</dd>` : itemTemplet += `<button type="button" class="layui-btn layui-btn-xs layui-btn-primary ${c}" lay-event="${option.type}" data-args='${a}'>${option.title}</button>`;
                }
                if (itemTemplet) templet += `<div class="layui-btn-group" style="margin-right: 10px">${itemTemplet}</div>`;
                if (dropdownTemplet) templet += dropdown.layout(dropdownTemplet);
            }
            return `<div class="layui-btn-container" yunj-id="${that.tableId}">${templet}</div>`;
        }

    }

    exports('TableColAction', TableColAction);
});