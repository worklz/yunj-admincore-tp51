/**
 * TableColImgs
 */
layui.define(['TableColTemplet','jquery','yunj'], function (exports) {

    let TableColTemplet = layui.TableColTemplet;
    let win = window;
    let doc = document;
    let $ = layui.jquery;

    class TableColImgs extends TableColTemplet{

        constructor(options) {
            super(options);
        }

        layout(){
            let that=this;
            return `{{# 
                         let imgs = d.${that.key};
                         if(yunj.isString(imgs)&&imgs.length>0) 
                            imgs = yunj.isJson(imgs)?JSON.parse(imgs):(imgs.indexOf(",")!==-1?imgs.split(","):[imgs]);
                         if(!yunj.isArray(imgs)) imgs = [];
                         let imgOnrrror = "";
                         let defaultSrc = "${that.args.default}";
                         if(defaultSrc) imgOnrrror = "this.src='"+defaultSrc+"';this.onerror=null;";
                     }}
                     {{# for(let i = 0,l = imgs.length;i < l;i++){ }}
                            {{#  if(d.is_export){  }}
                            {{ (i>0?'、':'')+imgs[i] }}
                            {{#  }else{  }}
                            <img class="table-row-imgs-item" src="{{ imgs[i] }}" alt="" title="点击预览" onerror="{{ imgOnrrror }}">
                            {{#  }  }}
                     {{# } }}`;
        }

        defineExtraEventBind(){
            let that=this;

            // 防止重复绑定事件
            if(yunj.isUndefined(win.TABLE_ROW_IMGS_ITEM_CLICK_PREVIEW_EVENT_BIND)){
                win.TABLE_ROW_IMGS_ITEM_CLICK_PREVIEW_EVENT_BIND = true;
                $(doc).on('click','.table-row-imgs-item',function (e) {
                    let currItemEl=$(this);
                    let srcArr=[];
                    currItemEl.parent().find('.table-row-imgs-item').each(function () {
                        srcArr.push($(this).attr('src'));
                    });
                    if(srcArr.length<=0) return false;
                    let idx=srcArr.indexOf(currItemEl.attr('src'));
                    if(idx===-1) idx=0;
                    yunj.previewImg(srcArr,idx);
                    e.stopPropagation();
                });
            }

        }

    }

    exports('TableColImgs', TableColImgs);
});