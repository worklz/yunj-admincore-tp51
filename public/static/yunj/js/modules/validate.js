/**
 * validate（验证器）
 * 传入scene，根据scene获取对应的校验规则checkRule
 * 不传scene，默认校验规则checkRule为所配置参数规则
 * 只校验校验规则checkRule内的参数
 */
layui.define(['yunj', "validateMethods"], function (exports) {

    let validateMethods = layui.validateMethods;

    class YunjValidate {

        constructor() {
            this._rule = {};     // 规则

            this._message = {};  // 提示消息

            this._scene = {};    // 验证环境

            this._error = [];    // 错误消息

            this._batch = false; // 是否批量验证

            this._methods = validateMethods;     // 验证方法

            this._methodsAppendRules = [];       // 验证方法补充的规则

            this._checkScene = "";      // 待验证环境

            this._checkRule = {};       // 待验证规则

            this._data = null;           // 待验证数据

            this._dataTitle = null;      // 待验证数据title
        }

        /**
         * 创建验证器
         * @param args
         * {
         *      rule:{
         *          "param1"=>"require|number",
         *          "param2"=>"require|number",
         *      },
         *      message:{
         *          "param1.require"=>"param1不能为空",
         *          "param1.number"=>"param1须由数字组成",
         *      },
         *      scene:{
         *          "test"=>["param1","param2"],
         *          "test2"=>["param1","param2"],
         *      },
         *      batch:true,
         *      methods:{
         *          test:(value,rule="",data={})=>{
         *              ...
         *              // 业务逻辑
         *              ...
         *              // 返回：bool|string
         *              return true;
         *          }
         *      }
         * }
         * @returns {YunjValidate}
         */
        create(args = {}) {
            let that = this;
            if (args.hasOwnProperty("rule")) that.rule(args.rule);
            if (args.hasOwnProperty("message")) that.message(args.message);
            if (args.hasOwnProperty("scene")) that.scene(args.scene);
            if (args.hasOwnProperty("batch")) that.batch(args.batch);
            if (args.hasOwnProperty("methods")) that.methods(args.methods);
            return that;
        }

        /**
         * 设置规则
         * @param rule
         * {
         *      "param1"=>"require|number",
         *      "param2"=>"require|number",
         * }
         * @returns {YunjValidate}
         */
        rule(rule = {}) {
            let that = this;
            that._rule = rule;
            return that;
        }

        /**
         * 设置提示消息
         * @param message
         * {
         *      "param1.require"=>"param1不能为空",
         *      "param1.number"=>"param1须由数字组成",
         * }
         * @returns {YunjValidate}
         */
        message(message = {}) {
            let that = this;
            that._message = message;
            return that;
        }

        /**
         * 设置验证场景
         * @param scene
         * {
         *      "test"=>["param1","param2"],
         *      "test2"=>["param1","param2"],
         * }
         * @returns {YunjValidate}
         */
        scene(scene = {}) {
            let that = this;
            that._scene = scene;
            return that;
        }

        /**
         * 设置错误消息
         * @param msg
         * @returns {YunjValidate}
         */
        setError(msg) {
            let that = this;
            that._error.push(msg);
            return that;
        }

        /**
         * 获取错误消息
         * @returns {string}
         */
        getError() {
            let that = this;
            return that._error.join(",");
        }

        /**
         * 设置是否批量验证
         * @param batch
         * @returns {YunjValidate}
         */
        batch(batch = false) {
            let that = this;
            that._batch = batch;
            return that;
        }

        /**
         * 追加验证规则对应方法
         * @param methods
         * {
         *      method1:(value,rule="",data={})=>{
         *          ...
         *          // 业务逻辑
         *          ...
         *          // 返回：bool|string
         *          return true;
         *      },
         *      method2:(value,rule="",data={})=>{
         *          ...
         *      }
         * }
         * @returns {YunjValidate}
         */
        methods(methods = {}) {
            let that = this;
            if (!yunj.isObj(methods)) {
                that.setError("[methods]值错误");
                return that;
            }
            let _methods = {};
            for (let k in methods) _methods[`method_${k}`] = methods[k];
            that._methods = Object.assign({}, _methods, that._methods);
            that._methodsAppendRules = Object.keys(methods);
            return that;
        }

        /**
         * 设置对应环境下的校验值
         * @param data  待校验数据对象
         * {
         *      "param1":"value1",
         *      "param2":"value2"
         * }
         * @param dataTitle   待校验数据对象 key 对应 title
         * {
         *      "param1":"参数一",
         *      "param2":"参数二"
         * }
         * @param scene 指定校验环境
         * @returns {YunjValidate}
         */
        setSceneData(data, dataTitle = {}, scene = "") {
            let that = this;
            // data
            if (!yunj.isObj(data)) {
                that.setError("[data]值需为键值对对象");
                return that;
            }
            // dataTitle
            if (!yunj.isObj(dataTitle)) {
                that.setError("[dataTitle]值需为键值对对象");
                return that;
            }
            // checkScene、checkRule
            let allRule = that._rule;
            if (scene) {
                if (!that._scene.hasOwnProperty(scene)) {
                    that.setError("[scene]环境值错误");
                    return that;
                }
                let checkRule = {};
                let sceneDataKeys = that._scene[scene];
                for (let i = 0, l = sceneDataKeys.length; i < l; i++) {
                    let k = sceneDataKeys[i];
                    if (!allRule.hasOwnProperty(k)) {
                        that.setError(`环境[${scene}]下数据[${k}]验证规则不存在`);
                        return that;
                    }
                    checkRule[k] = allRule[k];
                }
                that._checkScene = scene;
                that._checkRule = checkRule;
            } else {
                that._checkScene = "";
                that._checkRule = allRule;
            }
            return that;
        }

        /**
         * 校验
         * @param data          待校验数据对象
         * @param dataTitle   待校验数据对象 key 对应 title
         * @param scene         指定校验环境
         * @returns {boolean}
         */
        check(data, dataTitle = {}, scene = "") {
            let that = this;
            that._error = [];
            that.setSceneData(data, dataTitle, scene);
            if (that._error.length > 0) return false;
            let checkRule = that._checkRule;
            for (let k in checkRule) {
                if (!checkRule.hasOwnProperty(k)) continue;
                let v = data[k];
                let ruleArr = checkRule[k].split("|");
                if (ruleArr.length < 0 || (ruleArr[0] !== "require" && (v === undefined || v === null || v === ""))) continue;
                for (let i = 0, l = ruleArr.length; i < l; i++) {
                    // 拿到规则
                    let rule = ruleArr[i];
                    let [method, param] = rule.indexOf(':') !== -1 ? rule.split(':') : [rule, ''];
                    let methodName = `method_${method}`;
                    // 验证规则不存在时交给后端去校验
                    if (!that._methods.hasOwnProperty(methodName)) continue;
                    // 进行校验
                    let res = that._methods[methodName](v, param, data);
                    if (res === true) continue;
                    // 错误提示消息设置
                    let msg;
                    if (yunj.isString(res) && that._methodsAppendRules.indexOf(method) !== -1) {
                        msg = res;
                    } else {
                        let msgKey = `${k}.${method}`;
                        msg = that._message.hasOwnProperty(msgKey) ? that._message[msgKey]
                            : ((dataTitle.hasOwnProperty(k) ? dataTitle[k] : k) + (yunj.isString(res) ? res : "错误"));
                    }
                    that.setError(msg);
                    // 是否批量验证
                    if (!that._batch) return false;
                }
            }
            return that._error.length <= 0;
        }

        /**
         * 校验，当有错误时弹窗提示
         * @param data  待校验数据对象
         * @param dataTitle   待校验数据对象 key 对应 title
         * @param scene 指定校验环境
         * @returns {boolean}
         */
        checkTips(data, dataTitle = {}, scene = "") {
            let that = this;
            let res = that.check(data, dataTitle, scene);
            if (res === true) return true;
            let error = that.getError();
            yunj.alert(error, 'warn');
            throw new Error(error);
        }

    }

    let validate = new YunjValidate();

    exports('validate', validate);
});