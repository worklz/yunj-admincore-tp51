/**
 * FormFieldPassword
 */
layui.define(['FormField'], function (exports) {

    let FormField = layui.FormField;

    class FormFieldPassword extends FormField {

        constructor(options={}) {
            super(options);
        }

        defineExtraArgs(){
            let that = this;
            return {
                placeholder:"",
                readonly:false
            };
        }

        layoutControl() {
            let that = this;
            let controlHtml = `<input type="password" name="${that.id}" ${that.args.readonly ? 'readonly' : ''}
                       placeholder="${that.args.placeholder}" value="" autocomplete="off" class="layui-input">`;
            return `<div class="layui-input-inline yunj-form-item-control">${controlHtml}</div>`;
        }

        setValue(val=''){
            let that=this;
            that.fieldBoxEl.find(`input:password[name=${that.id}]`).val(val);
        }

        getValue(){
            let that=this;
            return that.fieldBoxEl.find(`input:password[name=${that.id}]`).val();
        }

    }

    exports('FormFieldPassword', FormFieldPassword);
});