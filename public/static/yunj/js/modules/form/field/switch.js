/**
 * FormFieldSwitch
 */
layui.define(['FormField', 'form'], function (exports) {

    let FormField = layui.FormField;
    let form = layui.form;

    class FormFieldSwitch extends FormField {

        constructor(options = {}) {
            super(options);
        }

        defineExtraArgs() {
            let that = this;
            return {
                text: "ON|OFF",
                disabled: false
            };
        }

        handleArgs(args) {
            if (args.verify.indexOf("boolean") === -1 && args.verify.indexOf("bool") === -1)
                args.verify += (args.verify ? "|" : "") + "boolean";
            return args;
        }

        layoutControl() {
            let that = this;
            let controlHtml = `<input type="checkbox" name="${that.id}" lay-text="${that.args.text}" } lay-skin="switch" lay-filter="${that.id}" ${that.args.disabled ? 'disabled' : ''}>`;
            return `<div class="layui-input-inline yunj-form-item-control yunj-input-pane">${controlHtml}</div>`;
        }

        setValue(val = '') {
            let that = this;
            that.fieldBoxEl.find(`input:checkbox[name=${that.id}]`).prop('checked', !!val);
            form.render('checkbox', that.tabFormFilter);
        }

        getValue() {
            let that = this;
            return that.fieldBoxEl.find(`input:checkbox[name=${that.id}]`).is(':checked');
        }

        renderDone() {
            let that = this;
            form.render('checkbox', that.tabFormFilter);
        }

    }

    exports('FormFieldSwitch', FormFieldSwitch);
});